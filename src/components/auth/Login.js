import { Text, Link, Box, Button, Center, FormControl, FormErrorMessage, FormLabel, Heading, Input, Flex, Spacer } from '@chakra-ui/react'
import React from 'react'
import { Link as RouterLink} from 'react-router-dom'
import { DASHBOARD, REGISTER } from 'lib/routes'
import { useLogin } from "hooks/auth"
import { useForm } from 'react-hook-form'
import { emailValidate, passwordValidate } from 'utils/form-validate'
import { BiSolidBrain } from 'react-icons/bi'

function Login() {
  const {login, isLoading} = useLogin();
  const { register, handleSubmit, reset, formState: { errors } } = useForm();

  async function handleLogin(data) {
    const succeeded = await login({
        email: data.email,
        password: data.password,
        redirectTo: DASHBOARD,
    });

    if (succeeded) reset();
  }

    return (
      <Flex
        h="100vh"
        align="center"
        justify="center"
      >
        <Box
          flex='1'
          w="80vw"
          h="100%"
          background="#C2DDF1"
        >
            <Flex
            direction='column'
            h='50%'
            ml='50'
            mt='30'
            >
                <Flex gap={2} align='center'>
                    <Box fontWeight='700' fontSize='2em' color='#335282'>brainDG</Box>
                    <BiSolidBrain color='#335282' fontSize='2.25em'/>
                </Flex>
                <Spacer />
                <Box color='#335282' fontWeight='600' fontSize='3em'>Why have one brain,</Box>
                <Box color='#335282' fontWeight='600' fontSize='3em'>when you can have <span style={{ fontWeight: "800" }}>more</span> </Box>
            </Flex>
        </Box>
        <Box
          background="#FFFFFF"
          p="4"
          width="550px"
        >
          <Box
            w="400px"
            margin="0 auto"
          >
            <Heading mb="4" size="lg" textAlign="center">
              Log In
            </Heading>
            <form onSubmit={handleSubmit(handleLogin)}>
              <FormControl isInvalid={errors.email} py="2">
                <FormLabel>Email</FormLabel>
                <Input
                  type="email"
                  placeholder="Email Address"
                  {...register("email", emailValidate)}
                />
                <FormErrorMessage>
                  {errors.email && errors.email.message}
                </FormErrorMessage>
              </FormControl>
              <FormControl isInvalid={errors.password} py="2">
                <FormLabel>Password</FormLabel>
                <Input
                  type="password"
                  placeholder="Password"
                  {...register("password", passwordValidate)}
                />
                <FormErrorMessage>
                  {errors.password && errors.password.message}
                </FormErrorMessage>
              </FormControl>
              <Button
                mt="4"
                type="submit"
                size="md"
                color='white'
                background='#335282'
                w="full"
                isLoading={isLoading}
                loadingText="Loading ..."
              >
                Log In
              </Button>
              <Text fontSize="xl" align="center" mt="6">
                Don't have an account?{" "}
                <Link
                  as={RouterLink}
                  to={REGISTER}
                  fontWeight="medium"
                  _hover={{ background: "blue.100" }}
                >
                  Register
                </Link>{" "}
                now!
              </Text>
            </form>
          </Box>
        </Box>
      </Flex>
    );
  }

  export default Login;
