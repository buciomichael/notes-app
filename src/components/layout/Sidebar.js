import { Box, Button, Code, Flex, Spacer, Stack, background } from '@chakra-ui/react'
import { useAuth, useLogout } from 'hooks/auth';
import { PROTECTED } from 'lib/routes';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import Avatar from 'components/profile/Avatar';
import { HiOutlineArrowNarrowLeft } from 'react-icons/hi'
import { CgDarkMode } from 'react-icons/cg'
import { PiCaretUpDown } from 'react-icons/pi'
import { VscDash } from 'react-icons/vsc'
import { BsSun } from 'react-icons/bs'
import { BsMoonStarsFill } from 'react-icons/bs'
import { BiSolidBrain } from 'react-icons/bi'
import { BiHeartCircle } from 'react-icons/bi'
import { useBrains } from 'hooks/brains';
import Favorites from './Favorites';


function ActiveUser() {
  const {user, isLoading} = useAuth();

  if (isLoading) return "Loading ..."

  return (
    <Stack
      align='center'
      spacing='5'
      my='8'
      mx='3'
     >
      <Avatar bg='red' user={user} />
      <Code color='var(--main_font_color)' bg='var(--username_background_color)' fontSize='.75em'>@{user?.username}</Code>
      <Button
      color='var(--main_font_color)'
      borderRadius='3'
      border="1px solid"
      borderColor="var(--border_color)"
      background='var(--bw_button_background)'
      as={Link}
      to={`${PROTECTED}/profile/${user?.id}`}
      w='full'
      fontSize='.75em'
      h='7'
      >
        Edit Profile
      </Button>
    </Stack>
  );
}

function Sidebar({ setOpen }) {

  const { logout, isLoading } = useLogout();
  const [ themeOpen, setThemeOpen ] = useState(false);
  const [ theme, setTheme ] = useState("light-mode");

  const handleOpenTheme = () => {
    setThemeOpen(!themeOpen);
  };

  const handleCloseSideNav = () => {
    setOpen(false);
  };

  const setDarkMode = () => {
    document.querySelector("body").setAttribute("data-theme", "dark-mode");
    setTheme("dark-mode")
    setThemeOpen(false)
  };

  const setLightMode = () => {
    document.querySelector("body").setAttribute("data-theme", "light-mode");
    setTheme("light-mode")
    setThemeOpen(false)
  };

  return (
    <Flex
      h="100vh"
      width='220px'
      borderRight="1px solid"
      borderColor='var(--border_color)'
      position='sticky'
      top='0'
      overflow='hidden'
      background='var(--background_color_2)'
    >
      <Flex
      direction='column'
      width='100%'
      >
        <Flex
         borderBottom="1px solid"
         borderColor='var(--border_color)'
         p='1'
         h='12'
         justifyContent='flex-end'
         align='center'>
          <Box color='var(--logo_color)' p='2'>
            <BiSolidBrain fontSize='1.75em' />
          </Box>
          <Spacer flex='1' />
          <Button onClick={() => handleCloseSideNav()} _hover={{ backgroundColor: "#E9E9E9" }} background='none'>
            <HiOutlineArrowNarrowLeft
              style={{ color:'lightgray', fontSize:'20px' }}
            />
          </Button>
        </Flex>
        <Box>
          <ActiveUser />
        </Box>
        <Box
          borderBottom='1px solid'
          borderTop='1px solid'
          borderColor='var(--border_color)'
          fontSize='.8em'
        >
          <Flex
            color='var(--main_font_color)'
            onClick={() => handleOpenTheme()}
            cursor='pointer'
            align='center'
            justifyContent='center'
            gap={3}
            px='2'
            py='3'
            _hover={{ backgroundColor: "var(--click_hover)" }}
          >
            <CgDarkMode fontSize='22px' />
            <Box fontWeight={600}>Theme</Box>
            <Spacer flex='1'></Spacer>
            <Box color='lightgrey' mr='2'>
              <PiCaretUpDown fontSize='15px' />
            </Box>
          </Flex>
          { themeOpen &&
            <Flex
            color='var(--main_font_color)'
            borderRadius='3'
            direction='column'
          >
            <Flex
            bg={theme === "light-mode" ? "var(--selected_background)" : "none"}
            cursor='pointer'
            onClick={() => setLightMode()}
            py='2'
            px='7'
            align='center'
            gap='5'
            _hover={theme === "light-mode" ? 'none' : { backgroundColor: "var(--click_hover)" }}
            >
              <BsSun />
              <Box fontSize='.95em'>Light</Box>
            </Flex>
            <Flex
            bg={theme === "dark-mode" ? "var(--selected_background)" : "none"}
            cursor='pointer'
            onClick={() => setDarkMode()}
            align='center'
            gap='5'
            py='2'
            px='7'
            _hover={theme === "dark-mode" ? 'none' : { backgroundColor: "var(--click_hover)" }}
            >
              <BsMoonStarsFill />
              <Box fontSize='.95em'>Dark</Box>
            </Flex>
          </Flex>}
        </Box>
        <Box
          color='var(--main_font_color)'
          h='200px'
          borderBottom='1px solid'
          borderColor='var(--border_color)'
          fontSize='.8em'
        >
          <Flex
            px='2'
            py='3'
            gap={3}
            align='center'
          >
            <BiHeartCircle fontSize='22px' color='var(--icon_color)' />
            <Box fontWeight={600} >Favorites</Box>
            <Spacer flex='1'></Spacer>
            <Box color='lightgrey' mr='2'>
              <PiCaretUpDown fontSize='15px' />
            </Box>
          </Flex>
          <Box pl={10}>
              <Favorites />
          </Box>
          </Box>
        <Spacer />
        <Button
        borderRadius='3'
        size='sm'
        fontSize='.8em'
        _hover={{ backgroundColor: '#3C3C3C' }}
        color='var(--logout_button_font)'
        background='var(--logout_button_background)'
        border='var(--logout_button_border)'
        borderColor='var(--logout_button_border_color)'
        mx='3'
        my='4'
        flexShrink='0'
        onClick={logout}
        isLoading={isLoading}
        >Logout</Button>
      </Flex>
    </Flex>
  )
}

export default Sidebar;
