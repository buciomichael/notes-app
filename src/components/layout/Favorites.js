import { Box } from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';


function Favorites() {

    const savedFavorites = JSON.parse(localStorage.getItem("favorite_brains")) || [];
    const brainFavorites = useSelector(state => state.brains.brainFavorites);

  return (
    <>
    <Box>
        {savedFavorites.map((brain) => (
            <Link key={brain.id} to={`/protected/brain/${brain.id}`}>
                <Box m={1}>- {brain.name}</Box>
            </Link>
        ))}
    </Box>
    </>
  )
}

export default Favorites
