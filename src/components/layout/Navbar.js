import { Flex, Button, Link, Box, FormControl, FormLabel, Input } from '@chakra-ui/react';
import { DASHBOARD } from 'lib/routes';
import { Link as RouterLink } from 'react-router-dom';
import Modal from 'react-modal';
import { useForm } from 'react-hook-form';
import { useAddBrain } from 'hooks/brains';
import { useAuth } from 'hooks/auth';
import { useState } from 'react';
import { AiFillCloseCircle } from 'react-icons/ai';
import { customStyles } from 'styles/ModalStyles';

Modal.setAppElement('#root');


function Navbar({ isHomePage }) {

    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
      setIsOpen(true);
    }

    function closeModal() {
      setIsOpen(false);
    }

    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const { addBrain, isLoading: addingBrain } = useAddBrain();
    const { user, isLoading: authLoading } = useAuth();

    function handleAddBrain(data) {
      addBrain({
        uid: user.id,
        name: data.name
      })
      reset();
      closeModal();
    }

      return (
        <Flex
          bg='var(--background_color_2)'
          borderBottom='1px solid'
          borderColor='var(--border_color)'
          w='100%'
          h="12"
          position='sticky'
          top='0'
        >
          <Flex pl='4' pr='2' w="100%" align="center">
            <Link
              flex='1'
              color='var(--main_font_color)'
              as={RouterLink} to={DASHBOARD}
              fontWeight={700}
              fontSize='1.1em'
            >
              Home
            </Link>
            <Box>
              <Button
                onClick={openModal}
                border='var(--color_button_border)'
                borderColor='var(--color_button_border_color)'
                borderRadius='3'
                color='black'
                fontSize='.8em'
                h="7"
                background='#F4AA39'
                size="sm"
                mr='3.5'
              >
                + New Brain
              </Button>
              <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
              >
                <Box textAlign='right'>
                  <button onClick={closeModal}>
                    <AiFillCloseCircle fontSize='1.5em' />
                  </button>
                </Box>
                <form onSubmit={handleSubmit(handleAddBrain)}>
                  <FormControl>
                    <FormLabel>Name</FormLabel>
                    <Input
                      max={40}
                      type='text'
                      placeholder='Brain Name'
                      {...register("name", { required: "A name is required." })}
                    />
                    <p>{errors.name?.message}</p>
                    <Button
                      color='white'
                      background='black'
                      w='full'
                      mt='3'
                      type='submit'
                      isLoading={authLoading || addingBrain}
                      loadingText='Loading'
                    >
                      Create
                    </Button>
                  </FormControl>
                </form>
              </Modal>
            </Box>
          </Flex>
        </Flex>
      );
    }

    export default Navbar;
