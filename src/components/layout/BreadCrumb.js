import { Link, Flex, Text, Box, Spacer, Tooltip, Button } from '@chakra-ui/react'
import { useBrain } from 'hooks/brains';
import { DASHBOARD } from 'lib/routes';
import React, { useEffect } from 'react'
import { Link as RouterLink, useParams, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";
import { addToFavorites, removeFromFavorites, setFavorites } from "app/brainSlice";
import { MdFavorite } from 'react-icons/md';
import { MdFavoriteBorder } from 'react-icons/md';


function BreadCrumb() {
  const { brainId } = useParams();
  const location = useLocation();
  const pathname = location.pathname;
  const { brain, isLoading } = useBrain(brainId);
  const dispatch = useDispatch();
  const brainFavorites = useSelector((state) => state.brains.brainFavorites);
  // .some will return a boolean
  const isFavorite = brainFavorites.some((favorite) => favorite.id === brain?.id);
  const isProfilePage = pathname.startsWith("/protected/profile/");

  const handleAddFavorite = () => {
      if (isFavorite) {
        dispatch(removeFromFavorites(brain));
        const updatedFavorites = brainFavorites.filter((favorite) => favorite.id !== brain.id);
        localStorage.setItem('favorite_brains', JSON.stringify(updatedFavorites));
      } else {
        dispatch(addToFavorites(brain));
        const updatedFavorites = [...brainFavorites, brain];
        localStorage.setItem('favorite_brains', JSON.stringify(updatedFavorites));
      }
  };

  useEffect(() => {
    const savedFavorites = JSON.parse(localStorage.getItem('favorite_brains')) || [];
    dispatch(setFavorites(savedFavorites));
  }, [dispatch]);

  return (
    <Flex
      bg="var(--background_color_2)"
      borderBottom="1px solid"
      borderColor="var(--border_color)"
      w="100%"
      h="12"
      position="sticky"
      top="0"
    >
      <Flex
        w='100%'
        px="4"
        align="center"
        gap={2}
        fontSize="1.1em"
      >
        <Flex gap={2}>
          <Link
            flex="1"
            color="var(--main_font_color)"
            as={RouterLink}
            to={DASHBOARD}
          >
            Home
          </Link>
            <Box
              color="var(--main_font_color)"
            >/
            </Box>
            <Text
              fontWeight={700}
              color="var(--main_font_color)"
            >
              {brain?.name}
              {isProfilePage &&
              <Text>Profile</Text>
              }
            </Text>
        </Flex>
        <Spacer flexGrow='1' />
        { !isProfilePage &&
        <Box fontSize={20} color='var(--main_font_color)' mr={1}>
          {isFavorite ? (
              <Tooltip mr={1} placement='bottom' label='Add to favorites' fontSize='sm'>
                <Button color="var(--main_font_color)" background='none' onClick={handleAddFavorite}>
                  <MdFavorite fontSize={20} />
                </Button>
              </Tooltip>
              ) : (
              <Tooltip label='Add to favorites' fontSize='sm'>
                <Button  color="var(--main_font_color)" background='none' onClick={handleAddFavorite}>
                  <MdFavoriteBorder fontSize={20} />
                </Button>
              </Tooltip>
              )}
        </Box>
        }
      </Flex>
    </Flex>
  );
}

export default BreadCrumb;
