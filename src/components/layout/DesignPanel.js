import { Button, Flex } from '@chakra-ui/react'
import { useAuth } from 'hooks/auth';
import { useAddBoard } from 'hooks/boards';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import { useForm } from "react-hook-form";


  function DesignPanel() {
    const { brainId } = useParams();

  return (
    <Flex
    background='var(--background_color_2)'
    justify='center'
    borderBottom='1px solid'
    borderColor='var(--border_color)'
    >
        <Flex
        align='center'
        h='70px'
        position='sticky'
        top='0'
        overflow='hidden'
        p='3'
        gap='8'
    >
        </Flex>
    </Flex>
  )
}

export default DesignPanel
