import { Box, Button, Flex } from '@chakra-ui/react'
import { HiOutlineArrowNarrowRight } from 'react-icons/hi'

function SidebarCollapse({ setOpen }) {

    const handleOpenSideNav = () => {
        setOpen(true);
      };

  return (
    <Flex
        bg='var(--background_color_2)'
        borderRight='1px solid'
        borderColor='var(--border_color)'
        h='100vh'
        w='75px'
        position='sticky'
        top='0'
        overflow='hidden'
    >
        <Flex
        direction='column'
        width='100%'
        >
            <Flex p='1' h='12' justifyContent='center' align='center'>
                <Button onClick={() => handleOpenSideNav()} _hover={{ background: "#E9E9E9" }} background='none'>
                    <HiOutlineArrowNarrowRight
                    style={{ color:'lightgray', fontSize:'20px' }}
                    />
                </Button>
            </Flex>
        </Flex>
    </Flex>
  )
}

export default SidebarCollapse
