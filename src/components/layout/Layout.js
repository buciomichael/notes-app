import { useAuth } from 'hooks/auth';
import Navbar from './Navbar';
import { LOGIN } from 'lib/routes';
import React, { useEffect, useState } from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import Sidebar from './Sidebar';
import { Box, Flex } from '@chakra-ui/react';
import DesignPanel from './DesignPanel';
import SidebarCollapse from './SidebarCollapse';
import BreadCrumb from './BreadCrumb';


function Layout() {
  const location = useLocation();
  const pathname = location.pathname;
  const navigate = useNavigate();
  const { user, isLoading } = useAuth();
  const [open, setOpen] = useState(true);


  useEffect(() => {
    if (!isLoading && pathname.startsWith("/protected") && !user) {
      navigate(LOGIN);
    }
  }, [pathname, user]);

  if (isLoading) return "Loading...";

  const isHomePage = pathname === "/protected/dashboard";
  const isProfilePage = pathname.startsWith("/protected/profile/");

  return (
    <Flex h='100vh'>
      {open ? (
        <Box>
          <Sidebar setOpen={setOpen} />
        </Box>
      ) : (
        <SidebarCollapse setOpen={setOpen} />
      )}
      <Flex direction="column" flex='1'>
        <Box flexShrink='0'>
          {isHomePage ? null : <BreadCrumb />}
          {isHomePage && <Navbar />}
        </Box>
        {!isHomePage && !isProfilePage && (
          <Box>
            <DesignPanel />
          </Box>
        )}
        <Box
          flex='1'
          p='3'
          background='var(--background_color)'
          overflowY='auto'
        >
          <Outlet />
        </Box>
      </Flex>
    </Flex>
  );
}

export default Layout;
