import React from 'react'
import { useAuth } from "hooks/auth";
import { Box, Flex, Spacer } from '@chakra-ui/react';
import { DndContext, MouseSensor, useSensor, useSensors} from '@dnd-kit/core';
import BrainsList from 'components/brain/BrainsList';
import Trash from './Trash';
import { useBrain, useDeleteBrain } from 'hooks/brains';


function Dashboard() {
    const { user: authUser } = useAuth();
    const { deleteBrain } = useDeleteBrain();
    const mouseSensor = useSensor(MouseSensor, {
        activationConstraint: {
        distance: 10,
        },
    });

    const sensors = useSensors(
        mouseSensor
    );

    async function handleDragEnd(event) {
        const { active, over } = event;
        const brainId = active.id
        if (over && over.id === 'trash') {
            await deleteBrain(brainId)
        }
      }

    if (authUser) {
        return (
            <DndContext
            sensors={sensors}
            onDragEnd={handleDragEnd}
            >
                <Flex
                h='100%'
                direction='column'
                background="var(--background_color)"
                overflow='hidden'
                >
                    <Box>
                        <BrainsList authUser={authUser} />
                    </Box>
                    <Spacer />
                    <Box
                    alignSelf='flex-end'
                    mr={3}
                    >
                        <Trash />
                    </Box>
                </Flex>
            </DndContext>
        );
    }

}

export default Dashboard
