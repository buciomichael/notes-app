import { Box, Flex, Text } from "@chakra-ui/react"
import { FcFullTrash } from 'react-icons/fc';
import { useDroppable } from '@dnd-kit/core';

function Trash() {

    const {isOver, setNodeRef} = useDroppable({
        id: 'trash',
      });
      const style = {
        color: isOver ? 'black' : 'lightgrey',
      };

  return (
    <Box
    ref={setNodeRef}
    style={style}
    >
        <Flex
        alignSelf='flex-end'
        direction='column'
        align='center'
        >
            <FcFullTrash fontSize='40px' />
            <Text fontSize='.8em' fontWeight={600}>Trash</Text>
        </Flex>
    </Box>
  )
}

export default Trash
