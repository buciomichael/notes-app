import { Box, Button, Flex, FormLabel, Input } from '@chakra-ui/react'
import { SortableContext, useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { collection, collectionGroup, getDocs, query, where } from 'firebase/firestore';
import { useAuth } from 'hooks/auth';
import { useBoard } from 'hooks/boards';
import { useAddCard, useBoardCards, useCards } from 'hooks/cards';
import { db } from 'lib/firebase';
import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import Modal from 'react-modal';
import { customBoardModal } from "styles/ModalStyles";

export default function Board({board, children}) {

    const {
        attributes,
        listeners,
        setNodeRef,
        transform,
        transition,
        isDragging
      } = useSortable({
        id: board.id,
      });
    const [modalIsOpen, setIsOpen] = useState(false);
    const { register, handleSubmit, reset } = useForm();
    const { user, isLoading: authLoading } = useAuth();
    const { addCard, isLoading: addingCard } = useAddCard();
    const style = {
        transform: CSS.Transform.toString(transform),
        transition,
      };

    function openModal() {
      setIsOpen(true);
    }

    function closeModal() {
      setIsOpen(false);
    }

    function handleAddCard(data) {
      addCard(board.id, {
        uid: user.id,
        title: data.title
      });
      reset();
      closeModal();
    }

  return (
    <>
    <Flex
    direction='column'
    >
      <Box
      fontWeight={500}
      fontSize='1.2em'
      >{board.title}</Box>
      <Box
      h='600px'
      w='200px'
      border='1px solid black'
      style={style}
      ref={setNodeRef}
      {...listeners}
      {...attributes}
      >
        {children}
      </Box>
      <Button
        onClick={openModal}
        m={2}
        background='lightgrey'
        >
          New Card
      </Button>
    </Flex>
    <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customBoardModal}
      >
        <Box textAlign='right'>
          <button onClick={closeModal}>
            X
          </button>
        </Box>
        <form
        onSubmit={handleSubmit(handleAddCard)}
        >
            <FormLabel>Card Title</FormLabel>
            <Input
            placeholder='Title'
            {...register("title")}
            />
            <Button
            type="submit"
            >
              Create
            </Button>
          </form>
      </Modal>
    </>
  )
}
