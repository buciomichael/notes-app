import { Box, Button, Flex, FormLabel, Input } from "@chakra-ui/react";
import { DndContext, KeyboardSensor, PointerSensor, closestCenter, useSensor, useSensors } from '@dnd-kit/core';
import { SortableContext, sortableKeyboardCoordinates } from "@dnd-kit/sortable";
import Board from "components/board";
import Card from "components/cards";
import { collection, collectionGroup, getDocs, query } from "firebase/firestore";
import { useAuth } from "hooks/auth";
import { useAddBoard, useBoards, useBoardsWithCards } from "hooks/boards";
import { useCards } from "hooks/cards";
import { db } from "lib/firebase";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Modal from 'react-modal';
import { customBoardModal } from "styles/ModalStyles";

export default function BrainDetail() {

  const { register, handleSubmit, reset } = useForm();
  const { user, isLoading: authLoading } = useAuth();
  const { addBoard, isLoading: addingBoard } = useAddBoard();
  const { boardsWithCards, isLoading: loadingBoards } = useBoardsWithCards();
  const [modalIsOpen, setIsOpen] = useState(false);
  console.log(boardsWithCards)

  function handleAddBoard(data) {
    addBoard({
      uid: user.id,
      title: data.title
    });
    reset();
    closeModal();
  }

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  // DND Handlers
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  );

    if (loadingBoards || addingBoard || authLoading) return "Loading...";

  return (
    <>
    <Button
      onClick={openModal}
      mb={4}
      background='lightgrey'
      >
        New Board
    </Button>
    <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
    >
      <SortableContext
      items={boardsWithCards}
      >
        <Flex gap={5}>
        {boardsWithCards.map((board) => (
          <Board key={board.id} id={board.id} board={board}>
            <SortableContext
            items={board.cards}
            >
              {board.cards.map((card) => (
                <Card key={card.id} id={card.id} card={card}/>
              ))}
            </SortableContext>
          </Board>
        ))}
        </Flex>
      </SortableContext>
    </DndContext>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customBoardModal}
      >
        <Box textAlign='right'>
          <button onClick={closeModal}>
            X
          </button>
        </Box>
        <form
        onSubmit={handleSubmit(handleAddBoard)}
        >
            <FormLabel>Board Title</FormLabel>
            <Input
            placeholder='Title'
            {...register("title")}
            />
            <Button
            type="submit"
            >
              Create
            </Button>
          </form>
      </Modal>
    </>
  );
}
