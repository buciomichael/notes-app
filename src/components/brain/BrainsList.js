import { Text, SimpleGrid, Center } from "@chakra-ui/react";
import Brain from ".";
import { useBrains } from "hooks/brains";

export default function BrainsList({ authUser }) {
  const { brains, isLoading } = useBrains();

  if (isLoading) return "Loading...";

  const userBrains = brains.filter((brain) => brain.uid === authUser.id);

  return (
      <Center mt={12}>
        {userBrains?.length === 0 ? (
          <Text textAlign="center" fontSize="lg">
            No brains yet ...
          </Text>
        ) : (
          <SimpleGrid columns={4} gap={8} minChildWidth={120} maxWidth={700}>
            {userBrains.map((brain) => (
                <Brain brain={brain} key={brain.id} />
            ))}
          </SimpleGrid>
        )}
      </Center>
  );
}
