import { Box, Flex } from "@chakra-ui/react";
import { LiaBrainSolid } from 'react-icons/lia';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setSelectedBrain } from "app/brainSlice";
import {useDraggable} from '@dnd-kit/core';
import { useState } from "react";

export default function Brain({ brain }) {
  const { name, id } = brain;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isDragging, setIsDragging] = useState(false);
  const { attributes, listeners, setNodeRef, transform } = useDraggable({
    id: id
  });
  const style = transform ? {
    transform: `translate3d(${transform.x}px, ${transform.y}px, 0)`,
  } : undefined;

  const handleDragStart = () => {
    dispatch(setSelectedBrain(brain));
    setIsDragging(true);
  };

  const handleDragEnd = () => {
    setIsDragging(false);
  };

  const handleClick = () => {
    if (!isDragging) {
      dispatch(setSelectedBrain(brain));
      navigate(`/protected/brain/${brain.id}`);
    }
  };

  return (
    <>
    <Box
      ref={setNodeRef}
      style={style}
      {...listeners}
      {...attributes}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onClick={handleClick}
    >
        <Flex
        direction='column'
        h='140px'
        align='center'
        justify='center'
        >
            <Flex
              borderRadius={10}
              align='center'
              justify='center'
              h='65px'
              w='65px'
              background='var(--brain_icon_background)'
            >
              <LiaBrainSolid fontSize='2.3rem' />
            </Flex>
            <Box
            mt={2}
            fontWeight={500}
            fontSize='.9em'
            color='var(--main_font_color)'
            textAlign='center'
            marginBottom='auto'
            >{name}
            </Box>
        </Flex>
    </Box>
    </>
  );
}
