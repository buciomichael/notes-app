import { HStack, Stack, Text } from '@chakra-ui/react';
import { useAuth } from 'hooks/auth';
import { useUser } from 'hooks/users';
import React from 'react'
import { format } from "date-fns";
import { useParams } from 'react-router-dom'

export default function Profile() {
    const { id } = useParams();
    const { user, isLoading: userLoading } = useUser(id);
    const { user: authUser, isLoading: authLoading } = useAuth();

    if (userLoading) return "Loading...";

  return (
    <>
    <Stack m={2}>
      <Text fontSize='1.1em' fontWeight={700}>Account Information</Text>
      <Text> Username: {user.username}</Text>
      <Text>
        Joined: {format(user.date, "MMMM YYY")}
      </Text>
    </Stack>
    </>
  )
}


{/* <Stack ml="10">
<Text fontSize="2xl">{user.username}</Text>
<HStack spacing="10">
  <Text color="gray.700" fontSize={["sm", "lg"]}>
    Brains: Pending
  </Text>
  <Text color="gray.700" fontSize={["sm", "lg"]}>
    Joined: {format(user.date, "MMMM YYY")}
  </Text>
</HStack>
</Stack> */}
