import { Avatar as ChakraAvatar } from '@chakra-ui/react'
import { PROTECTED } from 'lib/routes'
import { Link } from 'react-router-dom'

function Avatar({user}) {
    if (!user) return "Loading ..."
  return (
<ChakraAvatar
    border='var(--color_button_border)'
    borderColor='var(--color_button_border_color)'
    as={Link}
    background='#B17BC2'
    name={user.username}
    to={`${PROTECTED}/profile/${user.id}`}
    size='lg'
    _hover={{ cursor: 'pointer', opacity: '0.8' }}
/>
  )
}

export default Avatar
