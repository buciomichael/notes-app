import { Box } from '@chakra-ui/react'
import React from 'react'
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

export default function Card({ card }) {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({
    id: card.id,
  });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <>
    <Box
      ref={setNodeRef}
      style={style}
      {...attributes}
      {...listeners}
    >
      {card.title}
    </Box>
    </>
  )
}
