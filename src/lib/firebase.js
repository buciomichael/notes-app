// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
import { getAuth, setPersistence, browserSessionPersistence } from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCfiQQNaSNDfl2nNFcxfE3te_FkWxbF0O8",
  authDomain: "notes-app-52445.firebaseapp.com",
  projectId: "notes-app-52445",
  storageBucket: "notes-app-52445.appspot.com",
  messagingSenderId: "1055237420407",
  appId: "1:1055237420407:web:aaa477b553776be6ac9d72",
  measurementId: "G-996MY8PMVH"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
setPersistence(auth, browserSessionPersistence);
export const storage = getStorage(app);
const analytics = getAnalytics(app);
