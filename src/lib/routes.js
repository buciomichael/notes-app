import Login from "components/auth/Login";
import Register from "components/auth/Register";
import BrainDetail from "components/brain/BrainDetail";
import Dashboard from "components/dashboard/Dashboard";
import Layout from "components/layout/Layout";
import Profile from "components/profile";
import { createBrowserRouter } from "react-router-dom"

export const ROOT = "/";
export const LOGIN = "/login";
export const REGISTER = "/register";
export const PROTECTED = "/protected";
export const DASHBOARD = "/protected/dashboard";
export const PROFILE = "/protected/profile/:id";
export const BRAIN_DETAIL = "/protected/brain/:brainId";

export const router = createBrowserRouter([
    { path: ROOT, element: "Public Root" },
    { path: LOGIN, element: <Login /> },
    { path: REGISTER, element: <Register /> },
    {
        path: PROTECTED,
        element: <Layout />,
        children: [
            {
                path: DASHBOARD,
                element: <Dashboard />
            },
            {
                path: PROFILE,
                element: <Profile />
            },
            {
                path: BRAIN_DETAIL,
                element: <BrainDetail />
            },
        ],
    },
]);
