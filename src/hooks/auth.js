import { useAuthState, useSignOut } from 'react-firebase-hooks/auth';
import { auth, db } from 'lib/firebase';
import { useEffect, useState } from 'react';
import { DASHBOARD, LOGIN } from 'lib/routes';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import { useToast } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { doc, getDoc, setDoc } from 'firebase/firestore';
import isUsernameExists from 'utils/isUsernameExist';

export function useAuth() {
    const [authUser, authLoading, error] = useAuthState(auth);
    const [isLoading, setLoading] = useState(true);
    const [user, setUser] = useState(null);

    useEffect(() => {
        async function fetchData() {
          setLoading(true);
          const ref = doc(db, "users", authUser.uid);
          const docSnap = await getDoc(ref);
          setUser(docSnap.data());
          setLoading(false);
        }

        if (!authLoading) {
          if (authUser) fetchData();
          else setLoading(false);
        }
      }, [authLoading]);

      return { user, isLoading, error };
    }

export function useLogin() {
    const [isLoading, setLoading] = useState(false);
    const toast = useToast();
    const navigate = useNavigate();

    async function login({ email, password, redirectTo = DASHBOARD }) {
        setLoading(true);

        try {
            await signInWithEmailAndPassword(auth, email, password);
            toast({
                title: "Welcome!",
                status: "success",
                isClosable: true,
                position: "top",
                duration: 3500,
            });
            navigate(redirectTo);
            return true;
        } catch (error) {
            toast({
                title: "Log in Failed",
                status: "error",
                description: error.message || "An error occurred during login.",
                isClosable: true,
                position: "top",
                duration: 3500,
            });
            return false;
        } finally {
            setLoading(false);
        }
    }

    return { login, isLoading };
}


export function useRegister() {

    const [isLoading, setLoading] = useState(false);
    const toast = useToast();
    const navigate = useNavigate();

    async function register({
        username,
        email,
        password,
        redirectTo = DASHBOARD
    }) {
        setLoading(true);
        const usernameExists = await isUsernameExists(username)

        if (usernameExists) {
            toast({
                title: "Username already exists",
                status: "error",
                isClosable: true,
                position: "top",
                duration: 4000,
            });
            setLoading(false);
        } else {
            try{
                const response = await createUserWithEmailAndPassword(auth, email, password)

                await setDoc(doc(db, "users", response.user.uid), {
                    id: response.user.uid,
                    username: username.toLowerCase(),
                    avatar: "",
                    date: Date.now(),
                });

                toast({
                    title: "Registration Succesful",
                    status: "success",
                    isClosable: true,
                    position: "top",
                    duration: 4000
                });

                navigate(redirectTo)
            } catch (error) {
                toast({
                    title: "Registration Failed",
                    status: "error",
                    isClosable: true,
                    position: "top",
                    duration: 4000
                });
            } finally {
                setLoading(false)
            }
        }
    }

    return {register, isLoading};
}


export function useLogout() {
    const [signOut, isLoading, error] = useSignOut(auth);
    const navigate = useNavigate();
    const toast = useToast();

    async function logout() {
        try {
            await signOut();
            toast({
                title: "Logout Successful",
                status: "success",
                isClosable: true,
                position: "top",
                duration: 4000
            });
            navigate(LOGIN);
        } catch (error) {
            // Handle any errors that occur during sign-out
            console.error("Logout failed:", error);
            toast({
                title: "Logout Failed",
                status: "error",
                description: error.message || "An error occurred during logout.",
                isClosable: true,
                position: "top",
                duration: 4000
            });
        }
    }

    return { logout, isLoading };
}
