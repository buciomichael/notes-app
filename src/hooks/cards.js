import { useEffect, useState } from "react"
import { uuidv4 } from '@firebase/util'
import { db } from "lib/firebase";
import { collection, doc, getDocs, setDoc } from "firebase/firestore";
import { useToast } from "@chakra-ui/react";
import { useCollectionData } from "react-firebase-hooks/firestore";

export function useAddCard() {
    const [isLoading, setLoading] = useState(false);
    const toast = useToast();

    async function addCard(boardId, card) {
        setLoading(true);
        const cardId = uuidv4();
        const boardRef = doc(db, 'boards', boardId);
        const cardsCollectionRef = collection(boardRef, 'cards');

        await setDoc(doc(cardsCollectionRef, cardId), {
            ...card,
            id: cardId,
            date: Date.now(),
            boardId
        });

        toast({
            title: 'Card added successfully!',
            status: 'success',
            isClosable: true,
            position: 'top',
            duration: 1800,
        });

        setLoading(false);
    }

    return { addCard, isLoading };
}

export async function useBoardCards(boardId) {
  const [cards, setCards] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const fetchCards = async () => {
    try {
      const cardsCollectionRef = collection(db, 'boards', boardId, 'cards');
      const querySnapshot = await getDocs(cardsCollectionRef);
      const fetchedCards = querySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));

      console.log('fetched cards', fetchedCards)
      setCards(fetchedCards);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching cards:', error);
      setLoading(false);
    }
  };

  // Fetch cards when the boardId changes
  useEffect(() => {
    if (boardId) {
      fetchCards();
    }
  }, [boardId]);

  return { cards, isLoading };
}
