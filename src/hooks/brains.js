import { useEffect, useState } from "react"
import { uuidv4 } from '@firebase/util'
import { db } from "lib/firebase";
import { collection, deleteDoc, doc, getDoc, orderBy, query, setDoc } from "firebase/firestore";
import { useToast } from "@chakra-ui/react";
import { useCollectionData } from "react-firebase-hooks/firestore"

export function useAddBrain() {
    const [isLoading, setLoading] = useState(false);
    const toast = useToast();

    async function addBrain(brain) {
        setLoading(true);
        const id = uuidv4();
        await setDoc(doc(db, "brains", id), {
            ...brain,
            id,
            date: Date.now(),
        });
        toast({
            title: "Brain added successfully!",
            status: "success",
            isClosable: true,
            position: "top",
            duration: 1800,
          });
        setLoading(false);
    }

    return { addBrain, isLoading }
}

export function useDeleteBrain() {
  const [isLoading, setLoading] = useState(false);
  const toast = useToast();

  async function deleteBrain(brainId) {
    setLoading(true);
    const brainDocRef = doc(db, "brains", brainId);
    await deleteDoc(brainDocRef);
    toast({
      title: "Brain deleted successfully!",
      status: "success",
      isClosable: true,
      position: "top",
      duration: 3500,
  });
  }
  return { deleteBrain, isLoading };
}

export function useBrains() {
    const q = query(collection(db, "brains"), orderBy('date', 'desc'));
    const [brains, isLoading, error] = useCollectionData(q);
    if (error) throw error;
    return { brains, isLoading };
}

export function useBrain(brainId) {
    const [brain, setBrain] = useState(null);
    const [isLoading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
      const fetchBrain = async () => {
        try {
          const brainDocRef = doc(db, "brains", brainId);
          const brainDocSnapshot = await getDoc(brainDocRef);

          if (brainDocSnapshot.exists()) {
            setBrain({ ...brainDocSnapshot.data(), id: brainDocSnapshot.id });
          } else {
            setError("Brain not found");
          }
        } catch (error) {
          setError("Error fetching brain: " + error.message);
        } finally {
          setLoading(false);
        }
      };

      fetchBrain();
    }, [brainId]);

    return { brain, isLoading, error };
  }
