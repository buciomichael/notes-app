import { useEffect, useState } from "react"
import { uuidv4 } from '@firebase/util'
import { db } from "lib/firebase";
import { collection, deleteDoc, doc, getDoc, getDocs, onSnapshot, orderBy, query, setDoc, where } from "firebase/firestore";
import { useToast } from "@chakra-ui/react";
import { useCollectionData, useDocumentData } from "react-firebase-hooks/firestore";

export function useAddBoard() {
    const [isLoading, setLoading] = useState(false);
    const toast = useToast();

    async function addBoard(board) {
        setLoading(true);
        const id = uuidv4();
        await setDoc(doc(db, "boards", id), {
            ...board,
            id,
            date: Date.now(),
        });
        toast({
            title: "Board added successfully!",
            status: "success",
            isClosable: true,
            position: "top",
            duration: 1800,
          });
        setLoading(false);
    }

    return { addBoard, isLoading }
}

export function useBoards() {
    const q = query(collection(db, "boards"), orderBy('date', 'desc'));
    const [boards, isLoading] = useCollectionData(q);
    return { boards, isLoading };
}

export function useBoardsWithCards() {
    const [boardsWithCards, setBoardsWithCards] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
      const unsubscribe = onSnapshot(collection(db, 'boards'), (snapshot) => {
        const boardsData = [];

        snapshot.docs.forEach(async (boardDoc) => {
          const board = boardDoc.data();
          const cardsQuery = query(collection(boardDoc.ref, 'cards'));

          const cardsSnapshot = await getDocs(cardsQuery);
          const cardsData = cardsSnapshot.docs.map((cardDoc) => ({
            id: cardDoc.id,
            ...cardDoc.data(),
          }));

          boardsData.push({
            id: boardDoc.id,
            ...board,
            cards: cardsData,
          });

          setBoardsWithCards([...boardsData]); // Update state for each board
        });

        setIsLoading(false);
      });

      return () => unsubscribe();
    }, []);

    return { boardsWithCards, isLoading };
  }

export function useBoard(boardId) {
    const q = query(collection(db, "boards"), where('id', '==', boardId));
    const [board, isLoading] = useDocumentData(q);
    return { board, isLoading };
}

export function useDeleteBoard() {
    const [isLoading, setLoading] = useState(false);
    const toast = useToast();

    async function deleteBoard(boardId) {
      setLoading(true);
      const boardDocRef = doc(db, "boards", boardId);
      await deleteDoc(boardDocRef);
      toast({
        title: "Board deleted successfully!",
        status: "success",
        isClosable: true,
        position: "top",
        duration: 3500,
    });
    }
    return { deleteBoard, isLoading };
  }
