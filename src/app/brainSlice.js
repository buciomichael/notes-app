import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  brainFavorites: [],
  selectedBrain: null,
};

const brainSlice = createSlice({
  name: 'brains',
  initialState,
  reducers: {
    addToFavorites: (state, action) => {
      const brainToAdd = action.payload;
      const existingIndex = state.brainFavorites.findIndex(
        (favorite) => favorite.id === brainToAdd.id
      );

      if (existingIndex === -1) {
        state.brainFavorites.push(brainToAdd);
      }
    },
    removeFromFavorites: (state, action) => {
      const brainIdToRemove = action.payload;
      const indexToRemove = state.brainFavorites.findIndex(
        (favorite) => favorite.id === brainIdToRemove.id
      );

      if (indexToRemove !== -1) {
        state.brainFavorites.splice(indexToRemove, 1);
      }
    },
    setSelectedBrain: (state, action) => {
      state.selectedBrain = action.payload;
    },
    setFavorites: (state, action) => {
      state.brainFavorites = action.payload;
    },
  },
});


export const {
  addToFavorites,
  removeFromFavorites,
  setSelectedBrain,
  setFavorites
} = brainSlice.actions;
export default brainSlice.reducer;
