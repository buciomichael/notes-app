import { configureStore } from '@reduxjs/toolkit'
import brainReducer from './brainSlice'

export const store = configureStore({
  reducer: {
    brains: brainReducer
  },
});
